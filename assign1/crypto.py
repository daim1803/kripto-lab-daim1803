#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
from operator import index
from pydoc import plain
from turtle import pos, st
from unittest import result
import utils

# Caesar Cipher
def caesar_shift_char(char_to_shift: str, type: str) -> str:
    if char_to_shift > 'Z' or char_to_shift < 'A':
        return char_to_shift 
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    shift = 3 if type == 'encrypt' else -3
    pos = (alphabet.index(char_to_shift) + shift) % 26
    if pos < 0:
        pos += 26
    return alphabet[pos]

def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.

    Add more implementation details here.
    """
    encrypted_text = ""
    for char_pos in range(len(plaintext)):
        encrypted_text += caesar_shift_char(plaintext[char_pos],'encrypt')
    return encrypted_text

def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.

    Add more implementation details here.
    """
    decrypted_text = ""
    for char_pos in range(len(ciphertext)):
        decrypted_text += caesar_shift_char(ciphertext[char_pos],'decrypt')
    return decrypted_text


# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    encrypted_text = ''
    keyword_length = len(keyword)
    plaintext_length = len(plaintext)
    for char_pos in range(plaintext_length):
        pos = alphabet.index(plaintext[char_pos])\
                        + alphabet.index(keyword[char_pos % keyword_length])
        encrypted_text += alphabet[pos % 26]
    return encrypted_text

def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.

    Add more implementation details here.
    """
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    decrypted_test = ''
    keyword_length = len(keyword)
    ciphertext_length = len(ciphertext)
    for char_pos in range(ciphertext_length):
        pos = alphabet.index(ciphertext[char_pos])\
            -alphabet.index(keyword[char_pos % keyword_length])
        decrypted_test += alphabet[pos % 26]
    return decrypted_test


# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here

def encrypt_scytale(plaintext: str, circumference: int) -> str:
    result = ''
    for i in range(circumference):
        char_pos = i
        while char_pos < len(plaintext):
            result += plaintext[char_pos]
            char_pos += circumference
    return result

def decrypt_scytale(ciphertext, circumference: int) -> str:
    result = encrypt_scytale(ciphertext, circumference-1)
    return result


def encrypt_railfence(plaintext: str, num_rails: int) -> str:
    result = ''
    for i in range(num_rails):
        char_pos = i
        while char_pos < len(plaintext):
            print(char_pos)
            result += plaintext[char_pos]
            char_pos += 2 * (num_rails-1-i) if num_rails-1-i > 0 else num_rails + 1
    return result

def decrypt_railfence(ciphertext, num_rails) -> str:
    return encrypt_railfence(ciphertext, num_rails)
